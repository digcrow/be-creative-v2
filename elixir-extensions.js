var gulp     = require('gulp');
var elixir   = require('laravel-elixir');
var dotenv   = require('dotenv').load();
var _        = require('underscore');
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');
var wiredep  = require('wiredep').stream;
var cleanCSS = require('gulp-clean-css');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var autoprefixer = require('gulp-autoprefixer');
var Task     = elixir.Task;
var config   = elixir.config;


require('./elixir-extensions');

    function getLibsJS(params){
    var data = {"libs":[
        {"name":"datatable","links":
            [
                "bower_components/gentelella/vendors/datatables.net/js/jquery.dataTables.min.js",
                "bower_components/gentelella/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js",
                "bower_components/gentelella/vendors/datatables.net-buttons/js/dataTables.buttons.min.js",
                "bower_components/gentelella/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js",
                "bower_components/gentelella/vendors/datatables.net-buttons/js/buttons.flash.min.js",
                "bower_components/gentelella/vendors/datatables.net-buttons/js/buttons.html5.min.js",
                "bower_components/gentelella/vendors/datatables.net-buttons/js/buttons.print.min.js",
                "bower_components/gentelella/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js",
                "bower_components/gentelella/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js",
                "bower_components/gentelella/vendors/datatables.net-responsive/js/dataTables.responsive.min.js",
                "bower_components/gentelella/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js",
                "bower_components/gentelella/datatables.net-scroller/js/datatables.scroller.min.js",
            ]},
        {"name":"fastclick","links":["bower_components/gentelella/vendors/fastclick/lib/fastclick.js"]},
        {"name":"validator","links":["bower_components/gentelella/vendors/validator/validator.min.js"]},
        {"name":"chartjs","links":["bower_components/gentelella/vendors/Chart.js/dist/Chart.js"]},
        {"name":"parsley","links":[
            "bower_components/gentelella/vendors/parsleyjs/dist/parsley.js",
            "bower_components/gentelella/vendors/parsleyjs/dist/i18n/fr.js",
            //"bower_components/gentelella/vendors/parsleyjs/dist/i18n/en.js",
        ]},
        {"name":"scrollbar","links":["bower_components/gentelella/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"]},
        {"name":"sweetalert2","links":["bower_components/sweetalert2/dist/sweetalert2.js"]},
        {"name":"icheck","links":["bower_components/gentelella/vendors/iCheck/icheck.js"]},
        {"name":"requirejs","links":["bower_components/gentelella/vendors/requirejs/require.js"]},
        {"name":"nprogress","links":["bower_components/gentelella/vendors/nprogress/nprogress.js"]},
        {"name":"select2","links":["bower_components/gentelella/vendors/select2/dist/js/select2.full.js"]},
        {"name":"transitionize","links":["bower_components/gentelella/vendors/transitionize/dist/transitionize.js"]},
        {"name":"switchery","links":["bower_components/gentelella/vendors/switchery/dist/switchery.js"]},
        {"name":"starrr","links":["bower_components/gentelella/vendors/starrr/dist/starrr.js"]},
        {"name":"skycons","links":["bower_components/gentelella/vendors/skycons/skycons.js"]},
        {"name":"raphael","links":["bower_components/gentelella/vendors/raphael/raphael.js"]},
        {"name":"pnotify","links":["bower_components/gentelella/vendors/pnotify/dist/pnotify.js"]},
        {"name":"pdfmake","links":[
            "bower_components/gentelella/vendors/pdfmake/build/pdfmake.js",
            "bower_components/gentelella/vendors/pdfmake/build/vfs_fonts.js",
        ]},
        {"name":"pace","links":["bower_components/gentelella/vendors/PACE/pace.js"]},
        {"name":"morris","links":["bower_components/gentelella/vendors/morris.js/morris.js"]},
        {"name":"moment","links":["bower_components/gentelella/vendors/moment/src/moment.js"]},
        {"name":"mocha","links":["bower_components/gentelella/vendors/mocha/mocha.js"]},
        {"name":"colorpicker","links":["bower_components/gentelella/vendors/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.js"]},
        {"name":"jszip","links":["bower_components/gentelella/vendors/jszip/dist/jszip.js"]},
        {"name":"sparkline","links":["bower_components/gentelella/vendors/jquery-sparkline/dist/jquery.sparkline.js"]},
        {"name":"smartwizard","links":["bower_components/gentelella/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js"]},
        {"name":"mousewheel","links":["bower_components/gentelella/vendors/jquery-mousewheel/jquery.mousewheel.js"]},
        {"name":"knob","links":["bower_components/gentelella/vendors/jquery-knob/jquery.knob.js"]},
        {"name":"tagsinput","links":["bower_components/gentelella/vendors/jquery-tagsinput/src/jquery.tagsinput.js"]},
        {"name":"inputmask","links":["bower_components/gentelella/vendors/jquery.inputmask/dist/jquery.inputmask.bundle.js"]},
        {"name":"fullcalendar","links":["bower_components/gentelella/vendors/fullcalendar/dist/fullcalendar.js"]},
        {"name":"bootstrap","links":["bower_components/gentelella/vendors/bootstrap/dist/js/bootstrap.min.js"]},
        {"name":"autosize","links":["bower_components/gentelella/vendors/autosize/dist/autosize.js"]},
        {"name":"wysiwyg","links":["bower_components/gentelella/vendors/bootstrap-wysiwyg/src/bootstrap-wysiwyg.js"]},
        {"name":"cropper","links":["bower_components/gentelella/vendors/cropper/dist/cropper.js"]},
        {"name":"dropzone","links":["bower_components/gentelella/vendors/dropzone/dist/dropzone.js"]},
        {"name":"autocomplete","links":["bower_components/gentelella/vendors/devbridge-autocomplete/dist/jquery.autocomplete.js"]},
        {"name":"gauge","links":["bower_components/gentelella/vendors/gauge.js/dist/gauge.js"]},
        {"name":"rangeslider","links":["bower_components/gentelella/vendors/ion.rangeSlider/js/ion.rangeSlider.js"]},
        {"name":"jquery","links":["bower_components/gentelella/vendors/jquery/dist/jquery.js"]},
        {"name":"googlecodeprettify","links":["bower_components/gentelella/vendors/google-code-prettify/bin/prettify.min.js"]},
        {"name":"gentelella","links":["bower_components/gentelella/build/js/custom.js"]},
        {"name":"eve","links":["bower_components/gentelella/vendors/eve/eve.js"]},
        {"name":"flot","links":[
            "bower_components/gentelella/vendors/Flot/jquery.flot.js",
            "bower_components/gentelella/vendors/Flot/jquery.flot.time.js",
            "bower_components/gentelella/production/js/flot/date.js",
        ]},
        {"name":"daterangepicker","links":["bower_components/gentelella/vendors/bootstrap-daterangepicker/daterangepicker.js"]},
        {"name":"progressbar","links":["bower_components/gentelella/vendors/bootstrap-progressbar/bootstrap-progressbar.js"]},
    ]};
    var newArray = [];

    for (var i = 0; i < data.libs.length; i++) {
        for (var p = 0; p < params.length; p++){
            if (params[p] == data.libs[i].name){
                for (var k = 0; k < data.libs[i].links.length; k++){
                    newArray.push(data.libs[i].links[k]);
                }

            }
        }
    }
    return newArray;
}

    function getLibsCSS(params){
    var data = {"libs":[
        {"name":"datatable","links":
            [
                "bower_components/gentelella/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css",
                "bower_components/gentelella/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css",
                "bower_components/gentelella/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css",
                "bower_components/gentelella/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css",
                "bower_components/gentelella/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css",
            ]},
        {"name":"validator","links":["bower_components/gentelella/vendors/validator/fv.css"]},
        {"name":"scrollbar","links":["bower_components/gentelella/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css"]},
        {"name":"sweetalert2","links":["bower_components/sweetalert2/dist/sweetalert2.css"]},
        {"name":"icheck","links":["bower_components/gentelella/vendors/iCheck/skins/all.css"]},
        {"name":"nprogress","links":["bower_components/gentelella/vendors/nprogress/nprogress.css"]},
        {"name":"select2","links":["bower_components/gentelella/vendors/select2/dist/css/select2.css"]},
        {"name":"switchery","links":["bower_components/gentelella/vendors/switchery/dist/switchery.css"]},
        {"name":"starrr","links":["bower_components/gentelella/vendors/starrr/dist/starrr.css"]},
        {"name":"pnotify","links":["bower_components/gentelella/vendors/pnotify/dist/pnotify.css"]},
        {"name":"pace","links":["bower_components/gentelella/vendors/PACE/themes/blue/pace-theme-minimal.css"]},
        {"name":"morris","links":["bower_components/gentelella/vendors/morris.js/morris.css"]},
        {"name":"mocha","links":["bower_components/gentelella/vendors/mocha/mocha.css"]},
        {"name":"colorpicker","links":["bower_components/gentelella/vendors/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.css"]},
        {"name":"smartwizard","links":["bower_components/gentelella/vendors/jQuery-Smart-Wizard/styles/smart_wizard.css"]},
        {"name":"tagsinput","links":["bower_components/gentelella/vendors/jquery-tagsinput/src/jquery.tagsinput.css"]},
        {"name":"fullcalendar","links":["bower_components/gentelella/vendors/fullcalendar/dist/fullcalendar.css"]},
        {"name":"bootstrap","links":["bower_components/gentelella/vendors/bootstrap/dist/css/bootstrap.css"]},
        {"name":"wysiwyg","links":["bower_components/gentelella/vendors/bootstrap-wysiwyg/css/style.css"]},
        {"name":"cropper","links":["bower_components/gentelella/vendors/cropper/dist/cropper.css"]},
        {"name":"dropzone","links":["bower_components/gentelella/vendors/dropzone/dist/dropzone.css"]},
        {"name":"rangeslider","links":["bower_components/gentelella/vendors/ion.rangeSlider/css/ion.rangeSlider.css"]},
        {"name":"googlecodeprettify","links":["bower_components/gentelella/vendors/google-code-prettify/bin/prettify.min.css"]},
        {"name":"gentelella","links":["bower_components/gentelella/build/css/custom.css"]},
        {"name":"animatecss","links":["bower_components/gentelella/vendors/animate.css/animate.css"]},
        {"name":"normalize","links":["bower_components/gentelella/vendors/normalize-css/normalize.css"]},
        {"name":"fontawesome","links":["bower_components/gentelella/vendors/font-awesome/css/font-awesome.css"]},
        {"name":"daterangepicker","links":["bower_components/gentelella/vendors/bootstrap-daterangepicker/daterangepicker.css"]},
        {"name":"progressbar","links":["bower_components/gentelella/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.css"]},
    ]};
    var newArray = [];

    for (var i = 0; i < data.libs.length; i++) {
        for (var p = 0; p < params.length; p++){
            if (params[p] == data.libs[i].name){
                for (var k = 0; k < data.libs[i].links.length; k++){
                    newArray.push(data.libs[i].links[k]);
                }

            }
        }
    }
    return newArray;
}

    elixir.extend('pluginsJS', function(options,fileName) {

        var jsFiles = getLibsJS(options),
            destination = 'public/js';

        new Task('pluginsJS', function() {
            return gulp.src(jsFiles)
                .pipe(concat( fileName + '.js'))
                .pipe(gulp.dest(destination))
                .pipe(rename( fileName + '.min.js'))
                .pipe(uglify())
                .pipe(gulp.dest(destination));
        });
    });
    elixir.extend('pluginsCSS', function(options,fileName) {

        var CssFiles = getLibsCSS(options),
            destination = 'public/css';

        new Task('pluginsCSS', function() {
            return gulp.src(CssFiles)
                .pipe(cleanCSS({debug: true}, function(details) {
                    console.log(details.name + ': ' + details.stats.originalSize);
                    console.log(details.name + ': ' + details.stats.minifiedSize);
                }))
                //.pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9'))
                .pipe(autoprefixer({browsers: ['last 2 versions','safari 5','ie 8','ie 9','opera 12.1','ios 6','android 4'],cascade: true}))
                .pipe(concat(fileName + '.min.css'))
                .pipe(gulp.dest(destination))
        });
    });

elixir.extend('wiredep', function(options) {
    options = _.extend({
        ignorePath:  /(\..\/)*node_modules\//,
        fileTypes: {
            js: {
                block: /(([ \t]*)\/\/\s*bower:*(\S*))(\n|\r|.)*?(\/\/\s*endbower)/gi,
                detect: {
                    js: /\s['"](.+js)['"]/gi,
                    css: /\s['"](.+css)['"]/gi
                },
                replace: {
                    js: '"{{filePath}}",',
                    css: '"{{filePath}}",'
                }
            },
            sass: {
                block: /(([ \t]*)\/\/\s*bower:*(\S*))(\n|\r|.)*?(\/\/\s*endbower)/gi,
                detect: {
                    css: /@import\s(.+css)/gi,
                    sass: /@import\s(.+sass)/gi,
                    scss: /@import\s(.+scss)/gi
                },
                replace: {
                    css: '@import {{filePath}}',
                    sass: '@import {{filePath}}',
                    scss: '@import {{filePath}}'
                }
            }
        }
    }, options);

    new Task('wiredep', function() {
        return gulp.src('gulpfile.js')
            .pipe(wiredep(options))
            .on('error', function(e) {
                new elixir.Notification().error(e, 'Wiring Bower Dependencies Failed!');
                this.emit('end');
            })
            .pipe(gulp.dest('./'));
    })
    new Task('wiredep', function() {
        return gulp.src('resources/assets/sass/main.scss', {base: './'})
            .pipe(wiredep(options))
            .on('error', function(e) {
                new elixir.Notification().error(e, 'Wiring Bower Dependencies Failed!');
                this.emit('end');
            })
            .pipe(gulp.dest('./'));
    })
        .watch('bower.json');
});


elixir.extend('minifyCss', function() {
    new Task('minifyCss', function() {
        return gulp.src('public/css/*.css', {base: './'})
            .pipe(cleanCSS({debug: true}, function(details) {
                console.log(details.name + ': ' + details.stats.originalSize);
                console.log(details.name + ': ' + details.stats.minifiedSize);
            }))
            .on('error', function(e) {
                new elixir.Notification().error(e, 'Min. CSS Failed!');
                this.emit('end');
            })
            .pipe(gulp.dest('./'));
    });
});

elixir.extend('compress', function() {
    new Task('compress', function() {
        return gulp.src('public/js/*.js', {base: './'})
            .pipe(uglify())
            .on('error', function(e) {
                new elixir.Notification().error(e, 'JS uglify Failed!');
                this.emit('end');
            })
            .pipe(gulp.dest('./'));
    });
});


elixir.extend('images', function(options) {
    /*config.img = {
        folder: 'images',
        outputFolder: config.versioning.buildFolder + '/img'
    };

    options = _.extend({
        progressive: true,
        interlaced : true,
        svgoPlugins: [{removeViewBox: false, cleanupIDs: false}],
        use: [pngquant()]
    }, options);

    new Task('imagemin', function () {
        var paths = new elixir.GulpPaths()
            .src(config.get('public.img.folder'))
            .output(config.get('public.img.outputFolder'));

        return gulp.src(paths.src.path)
            .pipe(imagemin(options))
            .on('error', function(e) {
                new elixir.Notification().error(e, 'Minifying Images Failed!');
                this.emit('end');
            })
            .pipe(gulp.dest(paths.output.path));
    });*/
    options = _.extend({
        progressive: true,
        interlaced : true,
        svgoPlugins: [{removeViewBox: false, cleanupIDs: false}],
        use: [pngquant()]
    }, options);

    new Task('images', function() {
        return gulp.src('public/images/*')
            .pipe(imagemin(options))
            .pipe(gulp.dest('public/images'))
    });
});
