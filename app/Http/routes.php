<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


// Youtube Auth & Callback URLS (Do not edit or remove)
Route::get('/youtube/auth', function () {});
Route::get('/youtube/callback', function () {});

Route::get('/', 'AppController@index');
Route::post('/insertion/' , 'AppController@postCreate');
Route::post('/getUser/' , 'AppController@getUserByID');
Route::get('/participation/{id}' , 'AppController@postDetail');


Route::group(['namespace' => 'Admin', 'prefix' => 'admin'], function () {
    Route::auth();
    Route::get('/', 'AdminController@posts');
    Route::get('posts/', 'AdminController@posts')->name('viewPosts');
    Route::get('participants/', 'AdminController@participants');\
    Route::get('postAccept/{id}', 'AdminController@post_accept');
    Route::get('postRefused/{id}', 'AdminController@post_refused');

    Route::get('edit-participant/{id}', 'AdminController@editParticipant');
    Route::post('edit-participant/update/{id}', 'AdminController@updateParticipant');
});

//Route::post('auth/facebookLogin/{offline?}', 'Auth\AuthController@facebookLogin');
//Route::post('signup', 'AppController@signup');

