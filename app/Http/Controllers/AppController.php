<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Post;
use App\Vote;
use App\Participant;

class AppController extends Controller{

    public function index(){

        $list = Post::where('status', 'accept')->paginate(9);
        return view('index')->with([
            'list'              => $list
        ]);
//        if (Request::ajax()) {
//            return \Response::json(\View::make('posts', array('list' => $list))->render());
//        }else{
//            return view('index')->with([
//                'list'              => $list
//            ]);
//        }
    }


    public function postDetail($id){
        $post= Post::where('id', $id)->first();

        if($post) {
            return view('post_detail')->with([
                'post' => $post
            ]);
        }
        //abort(404);
    }

    public function getUserByID (Request $request)
    {
        $participant = Participant::where('facebook_id', $request->input('id') )->first();

        if ($participant <> null) {
            $participantPost = Post::where('participant_id', $participant->id )->first();
            if ($participantPost <> null) {
                return response()->json(['success' => false, 'message' => 'Vous avez déjà participé, Rendez-vous le 25 Mai.']);
            }
            Participant::where('id', $participant->id )->delete();
        }

        return response()->json(['success' => true, 'message' => 'ok']);
    }


    public function postCreate(Request $request){

        $validator = \Validator::make($request->all(), [
            'name'              => 'required',
            'agency'            => 'required',
            'post_title'        => 'required',
            'video'             => 'required|mimetypes:video/mp4,video/x-flv,video/x-msvideo,video/avi,video/mpeg,video/quicktime,video/MP2T,application/x-mpegURL,video/3gpp|max:80000',
            'birth_date'        => 'required',
            'email'             => 'required|email|unique:participants',
            'phone'             => 'required|regex:/^[0-9]{8}$/',
            'has_passport'      => 'required',
//            'has_visa'          => 'required'
        ]);

        $validator->setAttributeNames([
            'name'              => 'Nom& prénom',
            'agency'            => 'Agence',
            'post_title'        => 'Poste occupé',
            'video'             => 'Vidéo',
            'birth_date'        => 'Date de naissance',
            'email'             => 'Email',
            'phone'             => 'N° de Téléphone',
            'has_passport'      => 'Passport'
        ]);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'code' => 102, 'message' => implode('<br>', $validator->errors()->all())]);
        }

        $participant = new Participant;
        $participant->facebook_id   = $request->input('facebook_id');
        $participant->name          = $request->input('name');
        $participant->agency        = $request->input('agency');
        $participant->post_title    = $request->input('post_title');
        $participant->birth_date    = $request->input('birth_date');
        $participant->email         = $request->input('email');
        $participant->phone         = $request->input('phone');
        $participant->has_passport  = ( $request->input('has_passport') <> null ) ? $request->input('has_passport') : false ;
        $participant->has_visa      = ( $request->input('has_visa') <> null ) ?  $request->input('has_visa') : false ;
        $participant->ip            = $request->ip();

        if ( !$participant->save() ) {
            return response('Une erreur s\'est produite, veuillez réessayer ultérieurement.', 500);
        }

        $post = new Post;
        $post->participant_id   = $participant->id;
        $post->status           = 'pending';

        $video =  $request->file('video');

        $fileExtention = $video->guessExtension();
        $videoFileName = $participant->id.'_'.Carbon::now()->timestamp . '.' . $fileExtention;

        $dirVideo = storage_path('app/videos/');
        if (!\File::isDirectory($dirVideo)) {
            \File::makeDirectory($dirVideo, 755, true);
        }

        $video->move(
            $dirVideo,
            $videoFileName
        );

        if(($youtubeVideo = \Youtube::upload(
                $dirVideo.''.$videoFileName,
                [
                    'title'         => $participant->name ." - Be Creative By BH",
                    'description'   => "La banque de l'Habitat vous offre la possibilité de participer au Young Lions 2017 à Cannes, une des plus prestigieuses manifestations du secteur !",
                    'tags' => [
                        'Be Creative',
                        'Banque de l\'Habitat'
                    ]
                ],
                'unlisted'
            )) != false ) {
            $post->video_link = $youtubeVideo->getVideoId();
        }

        if(!$post->save()) {
            return response()->json([
                'success'   => false,
                'code'      => '3',
                'message'   => "Un problème est survenu lors de l'enregistrement de votre participation. Veuillez réessayer. Merci"
            ]);
        }

        return response()->json([
            'success'   => true,
            'message'   => "Merci pour votre participation, Votre vidéo sera en ligne aprés modération le 27 Mai."
        ]);
    }


    public function vote(Request $request){
        $vote = new Vote();
        $participant_id = $request->get('participant_id');

        $vote->participant_id = (integer) $participant_id;
        $vote->voter_id = session('participantId');

        if (!$vote->participant_id) {
            return response('Votre session a été expirée, veuillez réessayer.', 500);
        }

        $exist = Vote::whereRaw('champion_id = ? and  participant_id = ?', [$vote->champion_id, $vote->participant_id] )->count();

        if($exist!=0){
            return response()->json(['success' => false, 'message' => 'Vous avez déjà voté pour ce candidat !', 'count' => $this->getVoteCount($vote->champion_id)]);
        }else{
            if($vote->save()){
                return response()->json(['success' => true ]);
            }
        }

        return response('Echec de l\'opération', 500);
    }
}
