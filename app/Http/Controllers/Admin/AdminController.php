<?php

namespace App\Http\Controllers;

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Http\Request;
use App\Http\Requests;
use Carbon\Carbon;
use App\Post;
use App\Participant;
use App\Category;

class AdminController extends Controller{

    public function __construct(){
        $this->middleware('auth');
    }

    public function post_accept($id){
        $post = Post::find($id);
        $post->status = "accept";
        $post->save();
        return redirect()->route('viewPosts');
    }

    public function post_refused($id){
        $post = Post::find($id);
        $post->status = "refused";
        $post->save();
        return redirect()->route('viewPosts');
    }
    
    public function index(){
        $p = new Participant();
        $nbParticipant = Participant::all()->count();
        $nbPost = Post::all()->count();
        $postDate = Post::select()
            ->get()
            ->groupBy('date');
        $participantMonth = Participant::select(\DB::raw('SUBSTR(created_at, 6, 2) as month'))
                                        ->orderBy('month')
                                        ->get()
                                        ->groupBy('month');
        $postMonth = Post::select(\DB::raw('SUBSTR(created_at, 6, 2) as month'))
                                        ->orderBy('month')
                                        ->get()
                                        ->groupBy('month');

        return view('admin.index')->with([
            'nbParticipant' => $nbParticipant,
            'nbPost' => $nbPost,
            'postDate' => $postDate,
            'participantMonth' => $participantMonth,
            'postMonth' => $postMonth
        ]);
    }
    
    public function categoryDetail($id){
        return view('admin.category_detail')->with([
            'category' => Category::find($id)
        ]);
    }

    public function categories(){
        $list = Category::all();
        return view('admin.category')->with([
            'list' => $list,
            'total' => $list->count()
        ]);
    }

    public function participants(){
        return view('admin.participant')->with([
            'list' => Participant::all()
        ]);
    }

    public function editParticipant($id, Request $request){
        $participant = Participant::find($id);
        return view('admin.editParticipant')->with([
            'participant' => $participant
        ]);
    }

    public function updateParticipant($id, Request $request){


        $validator = \Validator::make($request->all(), [
            'participant_nom'  =>  'required',
            'participant_email' =>  'required|email',
            'participant_prenom' =>  'required',
            'facebook_id'  =>  'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['success' => false, 'message' => implode('<br>', $validator->errors()->all())]);
        }else{
            Participant::where('id', $id)
                ->update([
                    'participant_nom' => $request->participant_nom,
                    'participant_prenom' => $request->participant_prenom,
                    'facebook_id' => $request->facebook_id,
                    'participant_email' => $request->participant_email
                ]);
            return view('admin.participant')->with([
                'list' => Participant::all()
            ]);
        }
    }

    public function posts(){
        $p = new Post();
        return view('admin.posts')->with([
            'nbPost' => Post::all()->count(),
            'listpending' => $p->getListByStatus("pending"),
            'listaccept' => $p->getListByStatus("accept"),
            'listrefused' => $p->getListByStatus("refused"),
            'nbPending' => $p->getNumberByStatus("pending"),
            'nbAccepted' => $p->getNumberByStatus("accept"),
            'nbRefused' => $p->getNumberByStatus("refused")
        ]);
    }

    public function postDetail($id){
        $post = Post::find($id);
        return view('admin.post_detail')->with(['post' => $post ]);
    }

    public function editCategory(){
        $cat = Category::find(\Request::input('id'));
        $cat->categorie_name = \Request::input('categorie_name');
        $cat->save();
        return redirect()->route('viewCategories');
    }
}
