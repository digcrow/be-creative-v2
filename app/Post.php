<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'posts';

    public function participant()
    {
        return $this->belongsTo('App\Participant');
    }


    public function getNumberByStatus($status)
    {
        return Post::where('status', $status)->count();
    }

    public function getListByStatus($status)
    {
        return Post::where('status', $status)->get();
    }


    public function getPostImage(){
        return sprintf(
            'https://img.youtube.com/vi/%s/0.jpg',
            $this->video_link
        );
    }
}
