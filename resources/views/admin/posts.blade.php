@extends('admin.layouts.blank')

@push('stylesheets')

<!--   Exemple to push style -->
<!--<link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">-->

@endpush

@section('main_container')

    <div class="right_col" role="main">
        <div class="row top_tiles">
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <div class="icon"><i class="fa fa-comments-o"></i></div>
                    <div class="count">{{ $nbPost }}</div>
                    <h3>Posts</h3>
                    <p>Total numbers of posts.</p>
                </div>
            </div>
        </div>
        <div class="x_panel">
            <div class="x_content">
                <div class="" role="tabpanel" data-example-id="togglable-tabs">
                    <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Pending <span class="badge">{{ $nbPending }}</span></a>
                        </li>
                        <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Accepted <span class="badge">{{ $nbAccepted }}</span></a>
                        </li>
                        <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Refused <span class="badge">{{ $nbRefused }}</span></a>
                        </li>
                    </ul>
                    <div id="myTabContent" class="tab-content">
                        <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                            @if($listpending!=null)
                                <div class="row postblock">
                                    <div class="col-md-12">
                                        @foreach($listpending as $key => $item)
                                            @if($key % 3 == 0)
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        @endif
                                        <div class="col-sm-6 col-md-4">
                                            <div class="thumbnail" >
                                                    <iframe width="100%" height="300" src="https://www.youtube.com/embed/{{ $item->video_link }}" frameborder="0" allowfullscreen></iframe>
                                                <div class="caption">
                                                    <div class="row post-detail">
                                                        <h4><a href="http://facebook.com/{{ $item->participant->facebook_id}}" target="_blank">{{ $item->participant->name }}</a> </h4>
                                                            <ul>
                                                                <li>Agnece : <b>{{ $item->participant->agency }}</b> | Post : <b>{{ $item->participant->post_title }}</b></li>
                                                                <li>Birthday : <b>{{ $item->participant->birth_date }}</b></li>
                                                                <li>Email : <b>{{ $item->participant->email }}</b> || Phone: <b>{{ $item->participant->phone }}</b></li>
                                                                <li>Passport : <b>{{ $item->participant->has_passport }}</b> || Visa: <b>{{ $item->participant->has_visa }}</b></li>
                                                            </ul>
                                                    </div>
                                                    <div class="row text-center">
                                                        <a href="{{ url('admin/postAccept/'.$item->id) }}" class="btn btn-success btn-product"><span class="glyphicon glyphicon-ok-sign"></span> Accept</a>
                                                        <a href="{{ url('admin/postRefused/'.$item->id) }}" class="btn btn-danger btn-product"><span class="glyphicon glyphicon-remove-sign"></span> Refuse</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            @else
                                <h3 class="text-center">Aucune post!!!</h3>
                            @endif
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                            @if($listaccept!=null)
                                <div class="row postblock">
                                    <div class="col-md-12">
                                        @foreach($listaccept as $key => $item)
                                            @if($key % 3 == 0)
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        @endif
                                        <div class="col-sm-6 col-md-4">
                                            <div class="thumbnail" >
                                                <iframe width="100%" height="300" src="https://www.youtube.com/embed/{{ $item->video_link }}" frameborder="0" allowfullscreen></iframe>
                                                <div class="caption">
                                                    <div class="row post-detail">
                                                        <h4><a href="http://facebook.com/{{ $item->participant->facebook_id}}" target="_blank">{{ $item->participant->name }}</a> </h4>
                                                        <ul>
                                                            <li>Agnece : <b>{{ $item->participant->agency }}</b> | Post : <b>{{ $item->participant->post_title }}</b></li>
                                                            <li>Birthday : <b>{{ $item->participant->birth_date }}</b></li>
                                                            <li>Email : <b>{{ $item->participant->email }}</b> || Phone: <b>{{ $item->participant->phone }}</b></li>
                                                            <li>Passport : <b>{{ $item->participant->has_passport }}</b> || Visa: <b>{{ $item->participant->has_visa }}</b></li>
                                                        </ul>
                                                    </div>
                                                    <div class="row text-center">
                                                        <a href="{{ url('admin/postRefused/'.$item->id) }}" class="btn btn-danger btn-product"><span class="glyphicon glyphicon-remove-sign"></span> Refuse</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            @else
                                <h3 class="text-center">Aucune post!!!</h3>
                            @endif
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
                            @if($listrefused!=null)
                                <div class="row postblock">
                                    <div class="col-md-12">
                                        @foreach($listrefused as $key => $item)
                                            @if($key % 3 == 0)
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        @endif
                                        <div class="col-sm-6 col-md-4">
                                            <div class="thumbnail" >
                                                <iframe width="100%" height="300" src="https://www.youtube.com/embed/{{ $item->video_link }}" frameborder="0" allowfullscreen></iframe>
                                                <div class="caption">
                                                    <div class="row post-detail">
                                                        <h4><a href="http://facebook.com/{{ $item->participant->facebook_id}}" target="_blank">{{ $item->participant->name }}</a> </h4>
                                                        <ul>
                                                            <li>Agnece : <b>{{ $item->participant->agency }}</b> | Post : <b>{{ $item->participant->post_title }}</b></li>
                                                            <li>Email : <b>{{ $item->participant->email }}</b> || Phone: <b>{{ $item->participant->phone }}</b></li>
                                                            <li>Birthday : <b>{{ $item->participant->birth_date }}</b> || Passport : <b>{{ $item->participant->has_passport }}</b> || Visa: <b>{{ $item->participant->has_visa }}</b></li>
                                                        </ul>
                                                    </div>
                                                    <div class="row text-center">
                                                        <a href="{{ url('admin/postAccept/'.$item->id) }}" class="btn btn-success btn-product"><span class="glyphicon glyphicon-ok-sign"></span> Accept</a>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            @else
                                <h3 class="text-center">Aucune post!!!</h3>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection