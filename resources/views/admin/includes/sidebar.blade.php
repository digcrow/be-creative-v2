<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="{{ url('admin/') }}" class="site_title"><i class="fa fa-paw"></i> <span>Dashboard App</span></a>
        </div>
        <div class="clearfix"></div>

        <!-- menu profile quick info -->
        <div class="profile">
            <div class="profile_pic">
                <img src="{{ asset('images/user.png') }}" alt="..." class="img-circle profile_img">
            </div>
            <div class="profile_info">
                <span>Welcome,</span>
                <h2>{{ Auth::user()->full_name }}</h2>
            </div>
        </div>
        <!-- /menu profile quick info -->
        <br />

        <!-- sidebar menu -->
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                    {{--<li>--}}
                        {{--<a href="{{ url('admin/') }}"><i class="fa fa-home"></i> Dashboard</a>--}}
                    {{--</li>--}}
                    <li id="posts">
                        <a href="{{ url('admin/') }}"><i class="fa fa-edit"></i> Posts</a>
                    </li>
                    <li>
                        <a href="{{ url('admin/participants') }}"><i class="fa fa-desktop"></i> Users</a>
                    </li>
                    {{--<li>--}}
                        {{--<a href="{{ url('admin/categories') }}"><i class="fa fa-table"></i> Categories </a>--}}
                    {{--</li>--}}
                </ul>
            </div>

        </div>
        <!-- /sidebar menu -->
    </div>
</div>