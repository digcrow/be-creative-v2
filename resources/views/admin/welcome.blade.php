@extends('admin.layouts.blank')

@push('stylesheets')

    <!--   Exemple to push style -->
    <!--<link href="../vendors/iCheck/skins/flat/green.css" rel="stylesheet">-->

@endpush

@section('main_container')

    <!-- page content -->
    <div class="right_col" role="main">

        <h1>Laravel BackEnd Start !!!</h1>

    </div>
    <!-- /page content -->

    <!-- footer content -->
    <footer>
        <div class="pull-right">
            Starter Site Admin Template by Box.agency
        </div>
        <div class="clearfix"></div>
    </footer>
    <!-- /footer content -->
@endsection

