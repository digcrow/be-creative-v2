<!doctype html>
<!--[if lt IE 9]>      <html class="no-js lt-ie9" lang="fr"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="fr"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>@yield('title'){{ Config::get('app.name') }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="box.agency" />

    <!-- Metta CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <!-- Use RealFaviconGenerator.net to generate favicons  -->
    @include('partials.favicon')

    <link rel="stylesheet" href="{{ asset('css/plugins-admin.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/admin.css') }}">

    @stack('stylesheets')

        <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script>
        BASE_URL    = '{{ url('/') }}';
        CURRENT_URL = '{{ request()->url() }}';
        APP_ENV     = '{{ app()->environment() }}';
    </script>
    <script src="{{ asset("js/modernizer.js") }}"></script>
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">

        @include('admin/includes/sidebar')

        @include('admin/includes/topbar')

        @yield('main_container')

        @include('admin/includes/footer')

    </div>
</div>

<script src="{{ asset("js/vendor-admin.js") }}"></script>
<script src="{{ asset("js/plugins-admin.min.js") }}"></script>
<script src="{{ asset("js/admin.js") }}"></script>

@stack('scripts')

</body>
</html>