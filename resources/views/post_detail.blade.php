<div class="post">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="thumbnail card-post cat-{{ $post->category_id }}" data-id="{{ $post->id }}">
                @if($post->post_media_type == "image")
                        <div class="pic-wrapper">
                                <div class="over-img"></div>
                                <img src="{{ $post->getMessageImageOptim() }}" class="img-responsive">
                        </div>
                @else
                        <iframe width="100%" height="300" src="https://www.youtube.com/embed/{{$post->post_media_link}}" frameborder="0" allowfullscreen></iframe>
                @endif

                <div class="caption">
                        <div class="post-author clearfix">
                                <div class="author-pic">
                                        <img src="//graph.facebook.com/v2.6/{{ $post->participant->facebook_id }}/picture?width=100&height=100" alt="">
                                </div>
                                <div class="author-name">{{ $post->participant->getFullname() }}<small>{{ $post->post_region }}</small></div>
                        </div>
                        <div class="post-detail">
                                <h3>{{ $post->post_titre }}</h3>
                                <p>{{ $post->post_text }}</p>
                        </div>
                        <div class="footer clearfix">
                                <span class="label-cat label">{{ $post->category->categorie_name }}</span>
                                <span class="date label">{{ $post->post_date }}</span>
                        </div>
                </div>
        </div>
</div>