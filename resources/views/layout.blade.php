<!doctype html>
<!--[if lt IE 9]>      <html class="no-js lt-ie9" lang="fr"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="fr"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title'){{ Config::get('app.name') }}</title>

    @include('partials.socialMetaTags', [
        'title' => 'Vous avez moins de 30 ans et vous faites partie du milieu de la Communication en Tunisie ?',
        'description' => "La banque de l'Habitat vous offre la possibilité de participer au Young Lions 2017 à Cannes, une des plus prestigieuses manifestations du secteur ! "
    ])

    <!-- Metta CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <!-- Use RealFaviconGenerator.net to generate favicons  -->
    <link rel="shortcut icon" type="image/png" href="/favicon.png">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700|Permanent+Marker" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('css/plugins-front.css') }}">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">

    @stack('stylesheets')

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script>
        BASE_URL    = '{{ url('/') }}';
        CURRENT_URL = '{{ request()->url() }}';
        FB_APP_ID   = '{{ env('FACEBOOK_APP_ID') }}';
        APP_ENV     = '{{ app()->environment() }}';
    </script>
    <script src="{{ asset("js/modernizr.js") }}"></script>
</head>
<body class="@yield('class')">
    <!--[if lt IE 9]>
    <div class="alert alert-dismissible outdated-browser show" role="alert">
        <h6>Votre navigateur est obsolète !</h6>
        <p>Pour afficher correctement ce site et bénéficier d'une expérience optimale, nous vous recommandons de mettre à jour votre navigateur.
            <a href="http://outdatedbrowser.com/fr" class="update-btn" target="_blank">Mettre à jour maintenant </a>
        </p>
        <a href="#" class="close-btn" title="Fermer" data-dismiss="alert" aria-label="Fermer">&times;</a>
    </div>
    <![endif]-->

    <!--  Loader -->
    <div class="loader">
        <div class="spinner">
            <small>   Veuillez Patienter </small>
        </div>
    </div>

    <!--  Loader -->
    <div class="progress-wrapper hide">
        <div class="container">
            <div class="col-md-6 col-md-offset-3 box">
                <h3 class="title">  Veuillez Patienter <br> <small>Vos données sont en cours d'envoi </small> </h3>
                <div class="progress">
                    <div class="progress-bar progress-bar-info active progress-bar-striped" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 7%;">
                        00%
                    </div>
                </div>
                <div class="stats-box">2Mo envoyés sur 10 Mo</div>
            </div>
        </div>
    </div>

    <!-- App Wrapper -->
    <div id="wrapper" class="main_container">
        <header class="header">
            <div class="container">
                <a class="navbar-brand" href="/">
                    <img src="{{ asset('images/logo.png') }}" height="55px" alt="logo">
                </a>
                <ul id="top-menu" class="nav navbar-nav navbar-right main_nav">
                    <li> <img src="{{ asset('images/prosdelacom.png') }}" height="55px"</li>
                </ul>
            </div>
        </header>
        <div class="container">
            <div class="cta-text">
                <div class="col-md-12">
                    <img src="{{ asset('images/be-creative.png') }}" alt="Be Creative By BH" class="becreative-logo">
                </div>
                <div class="col-md-6">
                    <h1 class="title">Vous avez moins de 30 ans et vous faites partie du milieu de la com en Tunisie ?</h1>
                    <p>La banque de l'Habitat vous offre la possibilité de participer au Young Lions 2017 à Cannes, une des plus prestigieuses manifestations du secteur ! </p>
                    </p>Tout ce que vous avez à faire c'est de préparer une courte vidéo de 2 min 30 max pour vous présenter et répondre à cette question avant 23h59 le vendredi 26 Mai : Pourquoi on devra vous choisir pour faire partie de <b>l'équipe nationale de la créativité</b> ? 　</p>
                    <button class="btn fb-connect">je participe </button>
                </div>
                <div class="col-md-6 video-wrapper">
                    <iframe width="100%" height="290px" src="https://www.youtube.com/embed/U_nFO9ukFmU" frameborder="0" allowfullscreen=""></iframe>
                </div>

            </div>

        </div>

    </div>
    <!-- /#wrapper -->
    <div id="addPostModal" class="modal animated fadeInDown"  tabindex="-1" role="dialog" aria-labelledby="addPostModal" aria-hidden="true" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body form">
                    <div class="profile-block text-center">
                        <div class="profile-picture"><img src="{{ asset('images/profile-pic-placeholder.png') }}" alt=""></div>
                    </div>
                    <div class="form-info-bloc text-center">
                        <h3 class="title">Merci de remplir le formulaire</h3>
                        <p>Pour participer, il faut etre en poste dans une agence de communication et avoir un passeport valide
                            <br><small>Nb : Le candidat pré-sélectionné devra déposer sa demande de visa, si elle n'est pas acceptée un autre candidat sera désigné par le jury pour le remplacer.</small></p>
                    </div>
                    <form class="clearfix form-participent" method="post" action="{{ url('insertion') }}" enctype="multipart/form-data" id="addPost" data-toggle="validator">
                        {{ csrf_field() }}
                        <input type="hidden" name="facebook_id" class="user-fb-id">
                        <div class="col-sm-12">

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">
                                        <span>Votre nom & prénom * :</span>
                                        <input type="text" name="name" required="required" class="form-control user-name" placeholder="Votre nom & prénom..." title="Ce champ est obligatoire">
                                    </label>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">
                                        <span>Votre date de naissance < 30 * :</span>
                                        <input type="text" data-theme="bh" data-lang="fr" data-min-year="1987" data-default-date="07-01-1987" data-max-year="2005" data-large-mode="true" data-format="m-Y" data-translate-mode="true" readonly id="birthday" name="birth_date" class="date-picker form-control col-sm-5" placeholder="Votre date de naissance..." title="Ce champ est obligatoire">
                                    </label>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">
                                        <span>Votre adresse email * :</span>
                                        <input type="email" name="email" required="required" class="form-control user-email" placeholder="Votre adresse email..." title="Email incorrect">
                                    </label>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">
                                        <span>Votre numéro de téléphone * :</span>
                                        <input type="tel" name="phone" required="required" class="form-control" placeholder="Votre numéro de téléphone..." maxlength="8" required pattern="^[25493][0-9]{7}$" title="Téléphone incorrect">
                                    </label>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">
                                        <span>Agence * :</span>
                                        <input type="text" name="agency" required="required" class="form-control" placeholder="Votre agence..." title="Ce champ est obligatoire">
                                    </label>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">
                                        <span>Poste occupé * :</span>
                                        <input type="text" name="post_title" required="required" class="form-control" placeholder="Le poste occupé, copyrighter..." title="Ce champ est obligatoire">
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">
                                        <span>Vous avez un passeport valide ? *</span><br>
                                        <label class="switch">
                                            <input type="checkbox" name="has_passport" required="required" title="Ce champ est obligatoire" value="1">
                                            <div class="slider round"></div>
                                        </label>
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">
                                        <span>Vous avez un visa Schengen valide ?</span> <br>
                                        <label class="switch">
                                            <input type="checkbox" name="has_visa" value="1">
                                            <div class="slider round"></div>
                                        </label>
                                    </label>
                                </div>
                            </div>

                            <div class="form-group post-media col-sm-12">
                                <label class="control-label">
                                    <span>Votre vidéo * :</span>
                                    <div class="dropzone-wrapper" id="preview">
                                        <div class="file"><i class="fa fa-file"></i></div>
                                        <div class="file-name">Cliquer ou placer votre vidéo ici</div>
                                        <div class="file-size">  Taille max : 80 Mo  </div>
                                        <input type="file" name="video" required="required" class="form-control dropzone-input">

                                    </div>

                                </label>
                            </div>

                            <div class="form-group col-sm-12">
                                <p class="info"><small>* Champ obligatoire</small></p>
                            </div>

                            <div class="form-group modal-btns clearfix">
                                <button type="submit" class="btn btn-success">Valider</button>
                                <button type="reset" class="btn btn-primary" data-dismiss="modal" aria-label="Close">Annuler</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="actionDetail modal fade" tabindex="-1" role="dialog" id="actionDetail">
        <div class="modal-dialog">
            <div class="modal-content">
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <footer class="footer">
        <div class="container">
            © Be Creative By BH 2017 {{-- - <a href="#">Reglement</a>--}}
            <div class="pull-right">
                <a href="http://facebook.com/BHTunisie" target="_blank">Suivez nous <i class="fa fa-facebook"></i></a>
            </div>
        </div>

    </footer>

    <!-- Scripts -->
    <script src="{{ asset("js/plugins-front.js") }}"></script>
    <script src="{{ asset("js/main.js") }}"></script>

    @stack('scripts')

    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', '{{  env('GOOGLE_ANALYTICS') }}', 'auto');
        ga('send', 'pageview');
    </script>

</body>
</html>