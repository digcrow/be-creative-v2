@extends('layout')

@push('stylesheets')

@section('main_container')

    <style>

    </style>


<div class="main_container">
    @if($list!=null)
        <div class="container">
            <h2 class="text-center">{{ $title_page }}</h2>
            <div class="row postblock">
                <div class="col-md-12">
                    @foreach($list as $key => $item)
                        @if($key % 3 == 0)
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    @endif
                    <div class="col-sm-6 col-md-4">
                        <div class="thumbnail" >
                            <h4 class="text-center"><span class="label label-info">{{ $item->category->categorie_name }}</span></h4>
                                <img src="{{--{{ asset('app/images/thumb/'.$item->post_media_link) }}--}}{{ $item->getMessageImageThumb() }}" class="img-responsive">
                            <div class="caption">
                                <div class="row post-detail">
                                    <h3>{{ $item->participant->getFullname() }}</h3>
                                    <p>{{ $item->post_text }}</p>
                                </div>
                                <div class="row text-center">
                                    <a class="btn btn-success btn-product"><span class="glyphicon glyphicon-ok-sign"></span> Accept</a>
                                    <a href="#" class="btn btn-danger btn-product"><span class="glyphicon glyphicon-remove-sign"></span> Refuse</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    @else
        <h3 class="text-center">Aucune post!!!</h3>
    @endif
</div>
@endsection