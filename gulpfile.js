var elixir = require('laravel-elixir');
require('./elixir-extensions');
require('./gulpfile-admin');
require('./gulpfile-front');

elixir(function(mix) {

    mix
        .copy('bower_components/gentelella/vendors/bootstrap/fonts', 'public/fonts')
        .copy('bower_components/gentelella/vendors/font-awesome/fonts', 'public/fonts')
        .copy('bower_components/modernizer/modernizr.js', 'public/js')
        .images()
        .wiredep()
        .minifyCss()
        .compress()
        .browserSync({
            proxy: process.env.APP_URL
        });

});
