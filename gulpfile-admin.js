var elixir = require('laravel-elixir');
require('./elixir-extensions');

elixir(function(mix) {

    mix
        .sass('admin.scss')
        .babel('admin.js', 'public/js/admin.js')

        .scripts([
            // bower:js
            'bower_components/jquery/dist/jquery.js',
            'bower_components/bootstrap-sass/assets/javascripts/bootstrap.js',
            // endbower
        ], 'public/js/vendor-admin.js', 'bower_components')

    .pluginsJS(['gentelella','pdfmake','chartjs','datatable','sparkline','flot'],'plugins-admin')
    .pluginsCSS(['bootstrap','gentelella','fontawesome','datatable'],'plugins-admin')
});
