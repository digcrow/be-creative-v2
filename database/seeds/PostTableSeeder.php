<?php

use Illuminate\Database\Seeder;
use App\Post;

class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Post::create([
            "participant_id"=> "1",
            "post_titre"=> "Mllamco et excepteur adipisicing anim",
            "post_text"=> "Nisi exercitation ullamco et excepteur adipisicing anim. Consectetur sunt consectetur ut et ea nulla do do dolor adipisicing irure tempor. Nisi labore ad eiusmod laboris. Eu eu ullamco sit culpa qui duis mollit eiusmod minim culpa Lorem ex irure.\r\n",
            "post_video_link"=> "http://loremflickr.com/g/320/200/paris,girl/all",
            "post_status"=> "accept",
            "post_ip"=> "217.31.33.101"
        ]);
        Post::create([
            "participant_id"=> "2",
            "post_titre"=> "Consectetur sunt consectetur ut et ea nulla",
            "post_text"=> "Nisi exercitation ullamco et excepteur adipisicing anim. Consectetur sunt consectetur ut et ea nulla do do dolor adipisicing irure tempor. Nisi labore ad eiusmod laboris. Eu eu ullamco sit culpa qui duis mollit eiusmod minim culpa Lorem ex irure.\r\n",
            "post_video_link"=> "E5ONTXHS2mM",
            "post_status"=> "accept",
            "post_ip"=> "217.31.33.101"
        ]);
    }
}
