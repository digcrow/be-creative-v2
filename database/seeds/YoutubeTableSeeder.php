<?php

use Illuminate\Database\Seeder;

class YoutubeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('youtube_access_tokens')->insert([
            'access_token' => '{
                "access_token":"ya29.GlxQBBE7rJfucpaXR59jus7CM3lpKp0J8oDFFrDgZHbp7b-7ClJ6GCflJHHNrXnBFkwfHMgD2b2sFvX_DE9UwooKlrcf698IrXLxdJZnIWHc_jW9HcQ55D_E7z6V5A",
                "expires_in":3600,
                "refresh_token":"1\/T5oYMxBVvb_5h-_acC_ZS5oMAGAgDtE5LupNa8MOvoc",
                "token_type":"Bearer",
                "created":1495293237
            }'
        ]);

    }
}
