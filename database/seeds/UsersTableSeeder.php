<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder{

    public function run(){
        DB::table('users')->delete();
        User::create([
            'full_name' => 'Super Admin',
            'username' => 'admin',
            'email' => 'rami.jegham@gmail.com',
            'password' => bcrypt('admin@2O16+=')
        ]);
    }
}
