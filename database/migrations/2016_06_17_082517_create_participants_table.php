<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParticipantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('participants', function (Blueprint $table){
            $table->increments('id');
            $table->string('facebook_id')->unique();
            $table->string('name');
            $table->string('agency');
            $table->string('post_title');
            $table->string('birth_date');
            $table->string('email');
            $table->string('phone');
            $table->string('gender');
            $table->boolean('has_passport')->default(false);
            $table->boolean('has_visa')->default(false);
            $table->ipAddress('ip');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
