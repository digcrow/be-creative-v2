<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('votes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('voter_id')->unsigned();
            $table->integer('participant_id')->unsigned();
            $table->timestamps();

            $table->foreign('voter_id','voter_vote_fk')
                ->references('id')
                ->on('voters');

            $table->foreign('participant_id','participant_vote_fk')
                ->references('id')
                ->on('participants');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
